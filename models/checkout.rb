class Checkout

  attr_reader :promotion_rules
  attr_accessor :items, :sum

  def initialize(promotion_rules)
    @promotion_rules = promotion_rules
    @items = []
    @sum = 0
  end

  def scan(item)
    @items << item
  end

  def total
    set_sum
    promotion_rules.each { |p| p.call(self) }
    sum.round(2)
  end

  def item_number(code)
    @items.count { |element| element.code == code }
  end

  private

  def set_sum
    @items.each { |i| @sum += i.price }
  end
end