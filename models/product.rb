class Product

  attr_reader :code, :name, :price, :currency

  def initialize(code, name, price, currency = '£')
    @code = code
    @name = name
    @price = price
    @currency = currency
  end
end