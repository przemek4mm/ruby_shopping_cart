Dir["./models/*.rb"].each {|file| require file }
Dir["./domain/promotional_systems/*.rb"].each {|file| require file }


class App
  def console
    co = Checkout.new(promotional_rules)
    co.scan(get_product('001'))
    co.scan(get_product('002'))
    co.scan(get_product('003'))
    printf "Total: #{co.total}\n"
  end

  def promotional_rules
    [
        PromotionalSystems::SumPercentages.new(options: {sum: 60, percentages: 10})
    ]
  end

  def products
    items = []
    items << Product.new('001', 'Red Scarf', 9.25)
    items << Product.new('002', 'Silver cufflinks', 45.00)
    items << Product.new('003', 'Silk Dress', 19.95)
  end

  def get_product(code)
    products.each do |i|
      return i if i.code == code
    end
  end
end

app = App.new
app.console