Dir["./models/*.rb"].each {|file| require file }
Dir["./domain/promotional_systems/*.rb"].each {|file| require file }

RSpec.describe "Price" do

  let(:product_1) { Product.new('001', 'Red Scarf', 9.25) }
  let(:product_2) { Product.new('002', 'Silver cufflinks', 45.00) }
  let(:product_3) { Product.new('003', 'Silk Dress', 19.95) }
  let(:promotional_rules) {[]}


  it "should be true" do
    expect(true).to eq(true)
  end

  context 'base test' do
    it 'should work with empty promotional_rules' do
      co = Checkout.new(promotional_rules)
      co.scan(product_1)
      co.scan(product_2)
      co.scan(product_3)
      expect(co.total).to eq(74.20)
    end
  end

  context 'test app requirements' do

    let(:promotional_rules) {[
        PromotionalSystems::ItemCount.new(options: {item_code: '001', new_price: 8.50, old_price: 9.25, item_number: 2}),
        PromotionalSystems::SumPercentages.new(options: {sum: 60, percentages: 10})
    ]}

    describe 'Percentage promotional' do
      it 'should return correct checkout sum' do
        co = Checkout.new(promotional_rules)
        co.scan(product_1)
        co.scan(product_2)
        co.scan(product_3)
        expect(co.total).to eq(66.78)
      end
    end

    describe 'Item promotional' do
      it 'should return correct checkout sum' do
        co = Checkout.new(promotional_rules)
        co.scan(product_1)
        co.scan(product_3)
        co.scan(product_1)
        expect(co.total).to eq(36.95)
      end
    end

    describe 'All promotional' do
      it 'should return correct checkout sum' do
        co = Checkout.new(promotional_rules)
        co.scan(product_1)
        co.scan(product_2)
        co.scan(product_1)
        co.scan(product_3)
        expect(co.total).to eq(73.76)
      end
    end
  end


end