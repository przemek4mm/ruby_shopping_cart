module PromotionalSystems
  class ItemCount

    attr_accessor :item_code, :new_price, :old_price, :item_number

    def initialize(options:)
      @item_code = options[:item_code]
      @new_price = options[:new_price]
      @old_price = options[:old_price]
      @item_number = options[:item_number]
    end

    def call(checkout)
      promotional_items = checkout.item_number(@item_code)
      checkout.sum = checkout.sum - ((@old_price - new_price) * promotional_items) if promotional_items >= @item_number
    end
  end
end