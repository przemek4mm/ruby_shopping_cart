module PromotionalSystems
  class SumPercentages

    attr_accessor :sum, :percentages

    def initialize(options:)
      @sum = options[:sum]
      @percentages = options[:percentages]
    end

    def call(checkout)
      checkout.sum = (checkout.sum * ((100.0 - @percentages)/100)) if checkout.sum > @sum
    end
  end
end